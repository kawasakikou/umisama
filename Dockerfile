FROM python:3.6
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
RUN sed -i 's/core.urlresolvers/urls/g' /usr/local/lib/python3.6/site-packages/emoji/templatetags/emoji_tags.py
ADD . /code/

