from django.contrib.auth.models import AbstractUser
from django.db import models


import sys
sys.path.append("../")
from tweet.models import Subject



class ClassUser(AbstractUser):
    subject = models.ManyToManyField(Subject, choices=((s.pk, s) for s in Subject.objects.all()))

