from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import ClassUser

import sys
sys.path.append("../")
from tweet.models import Subject


class SignUpForm(UserCreationForm):
    subject = forms.ModelMultipleChoiceField(
        queryset=Subject.objects.all(),
        label='subject',
        help_text='参加している授業を選択して下さい',
        widget=forms.CheckboxSelectMultiple())

    class Meta:
        model = ClassUser
        fields = ('username', 'subject')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.save()
        user.subject.set(self.data.getlist('subject'))
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
