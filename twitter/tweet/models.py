from django.db import models
from django.utils import timezone
from django.utils.deconstruct import deconstructible


class Post(models.Model):
    user           = models.ForeignKey('accounts.ClassUser', on_delete=models.CASCADE)
    text           = models.TextField()
    created_date   = models.DateTimeField(default=timezone.now)
    times          = models.IntegerField(default=1)
    subject        = models.ForeignKey('Subject', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.text


class FixPost(models.Model):
    title          = models.TextField()
    text           = models.TextField()
    created_date   = models.DateTimeField(default=timezone.now)
    times          = models.IntegerField(default=1)
    subject        = models.ForeignKey('Subject', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.title


@deconstructible
class Subject(models.Model):
    title    = models.CharField(max_length=100, null=True)
    times    = models.IntegerField()


    def __str__(self):
        return self.title

