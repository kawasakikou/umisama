from django.urls import path
from . import views

app_name = 'tweet'
urlpatterns = [
    path('post/lists',                     views.PostView.as_view(),    name ='post_list'),
    path('<str:subject_name>/<int:times>', views.PostCreate.as_view(),  name ='class_post_new'),
    path('', views.RedirectFirstSubject.as_view(),      name ='class_default_new'),
]