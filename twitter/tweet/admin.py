from django.contrib import admin

# Register your models here.
from .models import Post, FixPost, Subject


class SubjectAdmin(admin.ModelAdmin):
    pass

admin.site.register(Subject, SubjectAdmin)
admin.site.register(FixPost)
admin.site.register(Post)
