from django.views import generic
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Post, Subject


class RedirectFirstSubject(LoginRequiredMixin, generic.base.RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        first_subject = self.request.user.subject.all()[0]
        self.url = '/' + first_subject.title + '/' + str(first_subject.times)
        return super().get_redirect_url(*args, **kwargs)


class PostView(LoginRequiredMixin, generic.ListView):
    context_object_name = 'posts'

    def get_queryset(self):
        return Post.objects.all()


class PostCreate(LoginRequiredMixin, generic.CreateView):
    model = Post
    fields = ['text']

    def form_valid(self, form):
        form.instance.user        = self.request.user
        form.instance.create_date = timezone.now
        form.instance.times       = self._get_subject_times()
        form.instance.subject     = self._get_context_subject()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        kwargs['user_object_list'] = self._get_post_filter_times().filter(user_id__gt=1)
        kwargs['root_object_list'] = self._get_post_filter_times().filter(user_id=1)
        kwargs['fix_post_list']    = self._get_fix_post_filter_times().all()
        kwargs['path']             = self.request.path
        kwargs['nav_title_times']  = self._get_subject_title() + '第' + str(self._get_subject_times()) + '回'
        return super(PostCreate, self).get_context_data(**kwargs)
    
    def get_success_url(self):
        return self.request.path

    def _get_post_filter_times(self):
        """postをsubject_timesでfilter"""
        return self._get_context_subject().post_set.filter(times=self._get_subject_times())

    def _get_fix_post_filter_times(self):
        """fixpostをsubject_timesでfilter"""
        return self._get_context_subject().fixpost_set.filter(times=self._get_subject_times())

    def _get_context_subject(self):
        return Subject.objects.filter(title=self._get_subject_title())[0]

    def _get_subject_title(self):
        """現在開いている教科のtitle取得 /math/2 => ['', 'math', '2'] => math """
        return self.request.path.split('/')[1]

    def _get_subject_times(self):
        """現在開いている教科の回数取得 /math/2 => ['', 'math', '2'] => 2 """
        return self.request.path.split('/')[2]
